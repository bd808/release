#!/usr/bin/env python3

import functools
import json
import logging
import os
import random
import sys
import re
import subprocess

BASE_RSYNC_ARGS = ["-a", "--stats", "--delete", "--delete-excluded"]


class App():
    def __init__(self, default_base_image, rsync_source, rsync_dest, output_image,
                 include=None,
                 exclude=None,
                 rsync_transfer_pct_max=25,
                 max_layers=125,  # Max layer count of 125 for overlayfs measured on Linux 4.19.0-14-amd64
                 full=False,
                 state_file=None,
                 report_file=None,
                 extra_commit_commands=None,
                 push=False,
                 ):
        """
        Parameters:
        default_base_image: The base image to use when incremental build is not suitable.
        rsync_source: The host directory to use as the rsync source.
        rsync_dest: The container directory to use as the rsync destination.
        output_image: The name of the output image to create.  The actual output image returned
                      may be different than this if nothing changed since the last build.
        include: A list of rsync patterns to include.
        exclude: A list of rsync patterns to exclude.
        rsync_transfer_pct_max': The maximum rsync transfer percentage to accept when determining
                                if incremental build is suitable.
        max_layers: The maximum number of layers that incremental build is allowed to create.
                    If exceeded, the default base image will be used.
        full: If true, disable incremental build.
        state_file: The name of a file to store incremental build information.
        report_file: The name of a file to store information about the build.
        extra_commit_commands: A list of extra Dockerfile commands to issue when committing
                               the new layer after the rsync.
        push: If true, push the output image to the registry after the build.
        """

        self.default_base_image = default_base_image
        self.rsync_source = rsync_source
        self.rsync_dest = rsync_dest
        self.output_image = output_image
        self.rsync_transfer_pct_max = rsync_transfer_pct_max
        self.max_layers = max_layers
        self.full = full
        self.state_file = state_file
        self.report_file = report_file
        self.extra_commit_commands = extra_commit_commands
        self.push = push

        self.state = self.read_state_file()

        self.rsync_filters = []
        if include:
            for path in include:
                self.rsync_filters.append("--include")
                self.rsync_filters.append(path)
        if exclude:
            for path in exclude:
                self.rsync_filters.append("--exclude")
                self.rsync_filters.append(path)

    def run(self):
        """
        Returns the output image name (which might not be the one supplied if no
        rebuild was needed).
        """

        base_image = self.select_base_image()
        logging.info("Using %s as the base image", base_image)

        # Save the original entrypoint of the base image since we override
        # it in the 'docker run' in run_build_container() and 'docker commit' saves that as
        # the entrypoint (if we don't do anything about it, but we do).
        original_entrypoint = get_image_entrypoint(base_image)

        stats = self.estimate_rsync(base_image)
        rsync_args = []
        report = {
            "base_image": base_image,
            "fresh": False,
            "pushed": False,
        }

        if stats["regular_files_transferred"] + stats["files_deleted"] + stats["files_created"] > 0:
            if stats["regular_files_transferred"] + stats["files_deleted"] <= 100:
                rsync_args.append("-v")

            logging.info("** rsync %s to container:%s **", self.rsync_source, self.rsync_dest)

            builder_id = self.run_rsync(base_image, rsync_args)
            try:
                logging.info("Commit %s", self.output_image)

                extra_cmds = []
                if self.extra_commit_commands:
                    for cmd in self.extra_commit_commands:
                        extra_cmds += ["-c", cmd]

                subprocess.check_call(["docker", "commit",
                                       "-c", "ENTRYPOINT {}".format(original_entrypoint)] + extra_cmds + [
                                           builder_id, self.output_image])

                report["fresh"] = True
                report["image"] = self.state["last_image"] = self.output_image
            finally:
                remove_container(builder_id)
        else:
            logging.info("Nothing to rsync from %s to %s", self.rsync_source, self.rsync_dest)
            report["image"] = self.state["last_image"] = base_image

        report["num_layers"] = count_image_layers(self.state["last_image"])

        self.update_state_file()

        if self.report_file:
            logging.debug("Writing report to %s", self.report_file)
            with open(self.report_file, "w") as f:
                json.dump(report, f, indent=4)
                f.write("\n")

        if self.push:
            push_image(report["image"])
            report["pushed"] = True

        logging.info("Image build finished")

        return report["image"]

    def read_state_file(self) -> dict:
        filename = self.state_file
        if filename is None or not os.path.exists(filename):
            return {}

        with open(filename) as f:
            return json.load(f)

    def update_state_file(self):
        filename = self.state_file
        if filename is None:
            return

        tmp_filename = f"{filename}.tmp"

        with open(tmp_filename, "w") as f:
            json.dump(self.state, f)
        os.rename(tmp_filename, filename)
        logging.debug("Updated state file %s", filename)

    def select_base_image(self) -> str:
        if self.full:
            logging.info("Full build requested. Using %s", self.default_base_image)
            return self.default_base_image

        image = self.state.get("last_image") or self.default_base_image

        if not self.image_is_suitable_as_base(image):
            logging.info("Falling back to %s", self.default_base_image)
            return self.default_base_image

        return image

    def image_is_suitable_as_base(self, image: str) -> bool:
        if image == self.default_base_image:
            # The default base image is always suitable.
            return True

        if not image_exists_locally(image):
            logging.info("%s does not exist locally", image)
            # Nonexistent images are not suitable.
            return False

        if not is_descendent_of(image, self.default_base_image):
            logging.info("%s is not a descendent of %s", image, self.default_base_image)
            return False

        num_layers = count_image_layers(image)

        if num_layers >= self.max_layers:
            logging.info(
                "Image %s is not suitable as a base image because it has %d layers (max is %d).",
                image,
                num_layers,
                self.max_layers)
            return False

        if self.rsync_transfer_pct_max is not None:
            rsync_transferpct = self.get_rsync_transfer_pct(image)
            if rsync_transferpct > self.rsync_transfer_pct_max:
                # The candidate base image doesn't provide sufficient data
                # transfer savings.
                logging.info(
                    "Image %s is not suitable due to rsync transfer pct %s (threshold is %s)",
                    image, rsync_transferpct, self.rsync_transfer_pct_max)
                return False

        # Looks ok
        return True

    def run_rsync(self,
                  image: str,
                  rsync_args=[],
                  capture_output=False,
                  quiet=False) -> str:
        """
        Start a container from 'image' and run rsync in it to copy from the source directory
        on the host to the destination directory in the container.

        If capture_output is True, the return value is a string containing the output
        of the rsync execution and the container is automatically removed after execution.

        If capture_output is False, the return value is the name of the (stopped) container
        where rsync executed.
        """

        container_id = generate_container_id()

        cmd = ["docker",
               "run",
               "--name={}".format(container_id),
               "--rm={}".format("true" if capture_output else "false"),
               "-v",
               "{}:/workdir:ro".format(self.rsync_source),
               "--entrypoint",
               "/usr/bin/rsync",
               image] + BASE_RSYNC_ARGS + rsync_args + self.rsync_filters + ["/workdir/",
                                                                             self.rsync_dest]
        if not quiet:
            logging.info("%s", " ".join(cmd))

        if capture_output:
            return subprocess.check_output(cmd, universal_newlines=True)

        try:
            subprocess.run(cmd, check=True)
        # Using BaseException here so that we can react to KeyboardInterrupt
        # properly.
        except BaseException as e:
            logging.error("Caught exception %s.\nTrying to stop container %s",
                          e, container_id)
            remove_container(container_id)
            raise

        return container_id

    @functools.lru_cache(maxsize=None)
    def estimate_rsync(self, image: str) -> dict:
        logging.info("Estimating rsync to %s", image)
        output = self.run_rsync(image,
                                rsync_args=["-n"],
                                capture_output=True,
                                quiet=True)

        return parse_rsync_stats(output)

    def get_rsync_transfer_pct(self, image: str) -> float:
        stats = self.estimate_rsync(image)

        return (stats["total_transferred_file_size"] /
                stats["total_file_size"]) * 100


def scos(cmd) -> str:
    """Subprocess Checked, Output Stripped"""
    return subprocess.check_output(cmd, universal_newlines=True).strip()


def generate_container_id() -> str:
    return "rsync-{}".format(random.randint(0, sys.maxsize))

# Copied from scap code


def parse_rsync_stats(string: str) -> dict:
    """
    Scans the string looking for text like the following and
    returns a dictionary with the extracted integer fields.

    Note that if no such matching text is found an empty dictionary
    will be returned.

    Number of files: 184,935 (reg: 171,187, dir: 13,596, link: 152)
    Number of created files: 0
    Number of deleted files: 0
    Number of regular files transferred: 1
    Total file size: 8,756,954,367 bytes
    Total transferred file size: 815,772 bytes
    Literal data: 0 bytes
    Matched data: 815,772 bytes
    File list size: 4,744,396
    File list generation time: 0.517 seconds
    File list transfer time: 0.000 seconds
    Total bytes sent: 5,603
    Total bytes received: 4,744,454
    """

    # Keys are header names expected from rsync --stats output.
    # Values are the names of the keys in 'res' that will be used.
    integer_fields = {
        "Number of files": "files",
        "Number of created files": "files_created",
        "Number of deleted files": "files_deleted",
        "Number of regular files transferred": "regular_files_transferred",
        "Total file size": "total_file_size",
        "Total transferred file size": "total_transferred_file_size",
        "Literal data": "literal_data",
        "Matched data": "matched_data",
        "File list size": "file_list_size",
        "Total bytes sent": "total_bytes_sent",
        "Total bytes received": "total_bytes_received",
    }

    res = {}

    for header, key in integer_fields.items():
        m = re.search(header + r": ([\d,]+)", string, re.MULTILINE)
        if m:
            res[key] = int(m.group(1).replace(",", ""))

    return res


def remove_container(id):
    subprocess.run(["docker", "rm", "-f", id], stdout=subprocess.DEVNULL)


def count_image_layers(image: str) -> int:
    return int(scos(["docker", "inspect", image,
                     "--format", "{{len .RootFS.Layers}}"]))


def get_image_entrypoint(image: str) -> str:
    return json.dumps(json.loads(scos(["docker", "image", "inspect", image]))[
                      0]["Config"]["Entrypoint"])


def image_exists_locally(image: str) -> bool:
    output = scos(["docker", "image", "ls", "-q", image])
    return output != ""


def get_image_id(image: str) -> str:
    return scos(["docker", "inspect", "-f", "{{.Id}}", image])


def get_parent(image: str) -> str:
    """
    Returns the id of the parent of 'image'.  If 'image' does not have a
    parent the result will be a blank string.
    """
    return scos(["docker", "inspect", "-f", "{{.Parent}}", image])


def is_descendent_of(image1, image2):
    image2_id = get_image_id(image2)

    image = image1
    while True:
        parent = get_parent(image)

        if not parent:
            return False

        if parent == image2_id:
            return True

        image = parent


def image_name_without_tag(image):
    m = re.match("(.*):.*$", image)
    if not m:
        raise ValueError(f"Image name '{image}' doesn't have a tag")
    return m[1]


def push_image(image):
    latest_image = image_name_without_tag(image) + ":latest"
    subprocess.check_call(["docker", "tag", image, latest_image])

    for name in [image, latest_image]:
        logging.info("Pushing %s", name)
        subprocess.check_call(["sudo", "/usr/local/bin/docker-pusher", "-q", name])
